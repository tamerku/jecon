package jecon.main.inf.adf;

import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealVector;
import org.apache.commons.math3.util.FastMath;

/**
 * class for augmented Dickey-Fuller test
 *
 * @author Tamer Kulaksizoglu
 */
public class DickeyFuller {

    private final AuxiliaryRegression areg;
    private int maximumLag;
    private int minimumLag;
    private final double[][] responseSurfaceTable = {
        {1, 1, -2.5649446, -2.3127399, -2.0306605, 0.00043039003, Double.NaN},
        {1, 5, -1.9407684, -0.33025231, -0.67821426, 0.00023323022, Double.NaN},
        {1, 10, -1.6167279, 0.21626789, -0.64616991, 0.00018128089, Double.NaN},
        {2, 1, -3.4301855, -6.4601835, -22.150622, 0.00040625530, Double.NaN},
        {2, 5, -2.8613794, -2.8583890, -6.5356937, 0.00022568290, Double.NaN},
        {2, 10, -2.5666852, -1.5407058, -2.6824738, 0.00017867718, Double.NaN},
        {3, 1, -3.9579703, -9.1969574, -22.702701, -188.39198, 0.00048941112},
        {3, 5, -3.4098271, -4.5119686, -3.6630374, -109.96942, 0.00026006414},
        {3, 10, -3.1266008, -2.6801993, 0.18064468, -70.566800, 0.00019867722}
    };
    private int selectedLag;

    public DickeyFuller() {
        areg = new AuxiliaryRegression();
        maximumLag = 0;
        minimumLag = 0;
        selectedLag = 0;
    }

    public void setDeterministic(Deterministic deterministic) {
        areg.setDeterministic(deterministic);
    }

    public void setDifference(Difference difference) {
        areg.setDiffference(difference);
    }

    public void setInformationCriteria(InformationCriteria informationCriteria) {
        areg.setInformationCriteria(informationCriteria);
    }

    public void setLags(int minimumLag, int maximumLag) {
        this.minimumLag = minimumLag;
        this.maximumLag = maximumLag;
    }

    public void setSeries(double[] series) {
        areg.setSeries(series);
    }

    public void run() {
        RealVector info = new ArrayRealVector(maximumLag - minimumLag + 1);
        for (int l = minimumLag; l <= maximumLag; l++) {
            areg.setLagStructure(l, maximumLag);
            areg.estimate();
            info.setEntry(l - minimumLag, areg.calculateInformationCriteria());
        }
        int minIndex = info.getMinIndex();
        selectedLag = minimumLag + minIndex;
        areg.setLagStructure(selectedLag, selectedLag);
        areg.estimate();
    }

    public double[] getCriticalValues() {
        double[] criticalValues = new double[3];
        int sampleSize = areg.getSampleSize();
        double[] vars = new double[]{1, FastMath.pow(sampleSize, -1), FastMath.pow(sampleSize, -2), FastMath.pow(sampleSize, -3)};
        Deterministic deterministic = areg.getDeterministic();
        switch (deterministic) {
            case NONE:
                for (int r = 0; r <= 2; r++) {
                    double sum = 0;
                    for (int c = 2; c <= 5; c++) {
                        sum += responseSurfaceTable[r][c] * vars[c - 2];
                    }
                    criticalValues[r] = sum;
                }
                break;
            case INTERCEPT:
                for (int r = 3; r <= 5; r++) {
                    double sum = 0;
                    for (int c = 2; c <= 5; c++) {
                        sum += responseSurfaceTable[r][c] * vars[c - 2];
                    }
                    criticalValues[r - 3] = sum;
                }
                break;
            case TREND_AND_INTERCEPT:
                for (int r = 6; r <= 8; r++) {
                    double sum = 0;
                    for (int c = 2; c <= 5; c++) {
                        sum += responseSurfaceTable[r][c] * vars[c - 2];
                    }
                    criticalValues[r - 6] = sum;
                }
                break;
        }
        return criticalValues;
    }

    public int getSampleSize() {
        return areg.getSampleSize();
    }

    public int getSelectedlag() {
        return selectedLag;
    }

    public double getTestStat() {
        return areg.getTestStat();
    }
}
