package jecon.main.inf.adf;

import org.apache.commons.math3.stat.regression.OLSMultipleLinearRegression;
import org.apache.commons.math3.util.FastMath;


/**
 * class for auxiliary regression for augmented Dickey-Fuller test
 *
 * @author Tamer Kulaksizoglu
 */
public class AuxiliaryRegression {

    private Deterministic deterministic;
    private Difference difference;
    private InformationCriteria informationCriteria;
    private int k;
    private int lag;
    private int maximumLag;
    private final OLSMultipleLinearRegression ols;
    private double rss;
    private int sampleSize;
    private double[] series;
    private double testStat;
    private double[] transformedSeries;

    /**
     * sets deterministic component
     *
     * @param deterministic
     */
    public void setDeterministic(Deterministic deterministic) {
        this.deterministic = deterministic;
    }

    /**
     * gets deterministic component
     *
     * @return
     */
    public Deterministic getDeterministic() {
        return deterministic;
    }

    /**
     * sets difference for series
     *
     * @param difference
     */
    public void setDiffference(Difference difference) {
        this.difference = difference;
    }

    /**
     * gets difference for series
     *
     * @return
     */
    public Difference getDifference() {
        return difference;
    }

    /**
     * sets information criteria for lag selection
     *
     * @param informationCriteria
     */
    public void setInformationCriteria(InformationCriteria informationCriteria) {
        this.informationCriteria = informationCriteria;
    }

    /**
     * gets information criteria for lag selection
     *
     * @return
     */
    public InformationCriteria getInformationCriteria() {
        return informationCriteria;
    }

    /**
     * sets lag structure
     *
     * @param lag
     * @param maximumLag
     */
    public void setLagStructure(int lag, int maximumLag) {
        this.lag = lag;
        this.maximumLag = maximumLag;
    }

    /**
     * gets lag
     *
     * @return
     */
    public int getLag() {
        return lag;
    }

    /**
     * gets maximum lag
     *
     * @return
     */
    public int getMaximumLag() {
        return maximumLag;
    }

    /**
     * gets sample size of auxiliary regression
     *
     * @return
     */
    public int getSampleSize() {
        return sampleSize;
    }

    /**
     * sets series
     *
     * @param series
     */
    public void setSeries(double[] series) {
        this.series = series;
    }

    /**
     * constructor
     */
    public AuxiliaryRegression() {
        deterministic = Deterministic.NONE;
        difference = Difference.LEVEL;
        informationCriteria = InformationCriteria.AKAIKE;
        k = 0;
        lag = 0;
        maximumLag = 0;
        ols = new OLSMultipleLinearRegression();
        ols.setNoIntercept(true);
        rss = Double.NaN;
        sampleSize = 0;
        series = null;
        testStat = Double.NaN;
        transformedSeries = null;
    }

    /**
     * calculates and returns information criteria
     *
     * @return
     */
    public double calculateInformationCriteria() {
        double logLik = -(((double) sampleSize) / 2) * (1 + FastMath.log(2 * FastMath.PI) + FastMath.log(rss / sampleSize));
        double crit;
        switch (informationCriteria) {
            case AKAIKE:
                crit = -2 * logLik / sampleSize + 2 * (double) k / sampleSize;
                break;
            case HANNAN_QUINN:
                crit = -2 * logLik / sampleSize + 2 * k * FastMath.log(FastMath.log(sampleSize)) / sampleSize;
                break;
            case SCHWARZ:
                crit = -2 * logLik / sampleSize + FastMath.log(sampleSize) * (double) k / sampleSize;
                break;
            default:
                crit = Double.NaN;
                break;
        }
        return crit;
    }

    /**
     * estimates auxiliary regression
     */
    public void estimate() {
        int n = series.length;
        switch (difference) {
            case FIRST_DIFFERENCE:
                transformedSeries = new double[n - 1];
                for (int t = 1; t < n; t++) {
                    transformedSeries[t - 1] = series[t] - series[t - 1];
                }
                break;
            case LEVEL:
                transformedSeries = new double[n];
                System.arraycopy(series, 0, transformedSeries, 0, n);
                break;
            case SECOND_DIFFERENCE:
                double[] firstDifferencedSeries = new double[n - 1];
                for (int t = 1; t < n; t++) {
                    firstDifferencedSeries[t - 1] = series[t] - series[t - 1];
                }
                transformedSeries = new double[n - 2];
                for (int t = 1; t < n - 1; t++) {
                    transformedSeries[t - 1] = firstDifferencedSeries[t] - firstDifferencedSeries[t - 1];
                }
                break;
        }
        if (lag == 0 && maximumLag == 0) {
            sampleSize = transformedSeries.length - 1;
            double[] lhs = new double[sampleSize];
            double[][] rhs = new double[sampleSize][];
            switch (deterministic) {
                case TREND_AND_INTERCEPT:
                    for (int t = 1; t < sampleSize + 1; t++) {
                        lhs[t - 1] = transformedSeries[t] - transformedSeries[t - 1];
                        k = 3;
                        rhs[t - 1] = new double[k];
                        rhs[t - 1][0] = transformedSeries[t - 1];
                        rhs[t - 1][1] = 1;
                        rhs[t - 1][2] = t;
                    }
                    break;
                case INTERCEPT:
                    for (int t = 1; t < sampleSize + 1; t++) {
                        lhs[t - 1] = transformedSeries[t] - transformedSeries[t - 1];
                        k = 2;
                        rhs[t - 1] = new double[k];
                        rhs[t - 1][0] = transformedSeries[t - 1];
                        rhs[t - 1][1] = 1;
                    }
                    break;
                case NONE:
                    for (int t = 1; t < sampleSize + 1; t++) {
                        lhs[t - 1] = transformedSeries[t] - transformedSeries[t - 1];
                        k = 1;
                        rhs[t - 1] = new double[k];
                        rhs[t - 1][0] = transformedSeries[t - 1];
                    }
                    break;
            }
            ols.newSampleData(lhs, rhs);
            double[] coef = ols.estimateRegressionParameters();
            double[] coefse = ols.estimateRegressionParametersStandardErrors();
            testStat = coef[0] / coefse[0];
            rss = ols.calculateResidualSumOfSquares();
        } else {
            double[] differencedSeries = new double[transformedSeries.length - 1];
            for (int t = 1; t < transformedSeries.length; t++) {
                differencedSeries[t - 1] = transformedSeries[t] - transformedSeries[t - 1];
            }
            double[] laggedSeries = new double[transformedSeries.length - 1];
            System.arraycopy(transformedSeries, 0, laggedSeries, 0, transformedSeries.length - 1);
            sampleSize = differencedSeries.length - maximumLag;
            double[] lhs = new double[sampleSize];
            double[][] rhs = new double[sampleSize][];
            switch (deterministic) {
                case TREND_AND_INTERCEPT:
                    for (int t = 0; t < sampleSize; t++) {
                        lhs[t] = differencedSeries[t + maximumLag];
                        k = lag + 3;
                        rhs[t] = new double[k];
                        rhs[t][0] = laggedSeries[maximumLag + t];
                        rhs[t][1] = 1;
                        rhs[t][2] = t + 1;
                        for (int l = 1; l <= lag; l++) {
                            rhs[t][l + 2] = differencedSeries[t + maximumLag - l];
                        }
                    }
                    break;
                case INTERCEPT:
                    for (int t = 0; t < sampleSize; t++) {
                        lhs[t] = differencedSeries[t + maximumLag];
                        k = lag + 2;
                        rhs[t] = new double[k];
                        rhs[t][0] = laggedSeries[maximumLag + t];
                        rhs[t][1] = 1;
                        for (int l = 1; l <= lag; l++) {
                            rhs[t][l + 1] = differencedSeries[t + maximumLag - l];
                        }
                    }
                    break;
                case NONE:
                    for (int t = 0; t < sampleSize; t++) {
                        lhs[t] = differencedSeries[t + maximumLag];
                        k = lag + 1;
                        rhs[t] = new double[k];
                        rhs[t][0] = laggedSeries[maximumLag + t];
                        for (int l = 1; l <= lag; l++) {
                            rhs[t][l] = differencedSeries[t + maximumLag - l];
                        }
                    }
                    break;
            }
            ols.newSampleData(lhs, rhs);
            double[] coef = ols.estimateRegressionParameters();
            double[] coefse = ols.estimateRegressionParametersStandardErrors();
            testStat = coef[0] / coefse[0];
            rss = ols.calculateResidualSumOfSquares();
        }
    }

    /**
     * gets test statistic
     *
     * @return
     */
    public double getTestStat() {
        return testStat;
    }
}
