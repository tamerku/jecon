package jecon.main.inf.adf;

/**
 * enumeration for series difference
 *
 * @author Tamer Kulaksizoglu
 */
public enum Difference {

    FIRST_DIFFERENCE,
    LEVEL,
    SECOND_DIFFERENCE
}
