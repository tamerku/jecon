package jecon.main.inf.adf;

/**
 * enumeration for deterministic components
 *
 * @author Tamer Kulaksizoglu
 */
public enum Deterministic {

    INTERCEPT,
    NONE,
    TREND_AND_INTERCEPT
}
