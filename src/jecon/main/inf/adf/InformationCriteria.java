package jecon.main.inf.adf;

/**
 * enumeration for information criteria
 *
 * @author Tamer Kulaksizoglu
 */
public enum InformationCriteria {

    AKAIKE,
    HANNAN_QUINN,
    SCHWARZ
}
