package jecon.main.util;

import org.apache.commons.math3.stat.regression.SimpleRegression;

/**
 * class for transforming data
 *
 * @author Tamer Kulaksizoglu
 */
public class DataTransformer {

    /**
     * demeans data
     *
     * @param data
     * @param demeanedData
     */
    public static void demean(double[] data, double[] demeanedData) {
        int n = data.length;
        double sum = 0;
        for (int i = 0; i < n; i++) {
            sum += data[i];
        }
        double mean = sum / n;
        for (int i = 0; i < n; i++) {
            demeanedData[i] = data[i] - mean;
        }
    }

    /**
     * detrends data
     *
     * @param data
     * @return detrended data
     */
    public static double[] detrend(double[] data) {
        SimpleRegression sr = new SimpleRegression();
        return detrend(data, sr);
    }

    /**
     * detrends data
     *
     * @param data
     * @param sr
     * @return
     */
    public static double[] detrend(double[] data, SimpleRegression sr) {
        sr.clear();
        int n = data.length;
        for (int i = 0; i < n; i++) {
            sr.addData(i + 1, data[i]);
        }
        sr.regress();
        double intercept = sr.getIntercept();
        double slope = sr.getSlope();
        double[] detrendedData = new double[n];
        for (int i = 0; i < n; i++) {
            detrendedData[i] = data[i] - intercept - slope * (i + 1);
        }
        return detrendedData;
    }

    /**
     * takes the first difference of the argument<br>
     * first element is NaN
     *
     * @param series
     * @return
     */
    public static double[] diff(double[] series) {
        int n = series.length;
        double[] diffSeries = new double[n];
        diffSeries[0] = Double.NaN;
        for (int i = 1; i < n; i++) {
            diffSeries[i] = series[i] - series[i - 1];
        }
        return diffSeries;
    }

    /**
     * takes first difference of series trimming missing values
     *
     * @param series series
     * @return first difference of series
     */
    public static double[] diffTrim(double[] series) {
        int n = series.length;
        double[] diffSeries = new double[n - 1];
        for (int i = 0; i < n - 1; i++) {
            diffSeries[i] = series[i + 1] - series[i];
        }
        return diffSeries;
    }

    /**
     * takes nth lag of series<br>
     * first n elements are filled with NaNs
     *
     * @param series series
     * @param n lag
     * @return nth lag of series
     */
    public static double[] lag(double[] series, int n) {
        int length = series.length;
        double[] laggedSeries = new double[length];
        for (int i = 0; i < Math.min(n, length); i++) {
            laggedSeries[i] = Double.NaN;
        }
        for (int i = n; i < length; i++) {
            laggedSeries[i] = series[i - n];
        }
        return laggedSeries;
    }

    /**
     * returns nth lag of series trimming missing values
     *
     * @param series series
     * @param n lag
     * @return nth lag of series
     */
    public static double[] lagTrim(double[] series, int n) {
        int length = series.length;
        double[] laggedSeries = new double[length - n];
        System.arraycopy(series, 0, laggedSeries, 0, length - n);
        return laggedSeries;
    }
}
