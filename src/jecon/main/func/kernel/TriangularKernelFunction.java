package jecon.main.func.kernel;

import org.apache.commons.math3.util.FastMath;

/**
 * class for uniform kernel function
 *
 * @author Tamer Kulaksizoglu
 */
public class TriangularKernelFunction extends AbstractKernelFunction {

    @Override
    public double valueAt(double z) {
        double absz = FastMath.abs(z);
        if (absz <= 1) {
            return 1 - absz;
        } else {
            return 0;
        }
    }
}
