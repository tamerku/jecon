package jecon.main.func.kernel;

import org.apache.commons.math3.util.FastMath;

/**
 * class for tricube kernel function
 *
 * @author Tamer Kulaksizoglu
 */
public class TricubeKernelFunction extends AbstractKernelFunction {

    private static final double constant = (double) 70 / 81;

    @Override
    public double valueAt(double z) {
        double absz = FastMath.abs(z);
        if (absz <= 1) {
            return constant * FastMath.pow(1 - FastMath.pow(absz, 3), 3);
        } else {
            return 0;
        }
    }
}
