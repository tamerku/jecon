package jecon.main.func.kernel;

/**
 * abstract class for kernel functions
 *
 * @author Tamer Kulaksizoglu
 */
public abstract class AbstractKernelFunction {

    public abstract double valueAt(double z);
}
