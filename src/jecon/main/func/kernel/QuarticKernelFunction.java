package jecon.main.func.kernel;

import org.apache.commons.math3.util.FastMath;

/**
 * class for quartic kernel function
 *
 * @author Tamer Kulaksizoglu
 */
public class QuarticKernelFunction extends AbstractKernelFunction {

    private static final double constant = (double) 15 / 16;

    @Override
    public double valueAt(double z) {
        if (FastMath.abs(z) <= 1) {
            return constant * FastMath.pow(1 - FastMath.pow(z, 2), 2);
        } else {
            return 0;
        }
    }
}
