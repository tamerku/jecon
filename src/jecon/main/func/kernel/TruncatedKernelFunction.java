package jecon.main.func.kernel;

import org.apache.commons.math3.util.FastMath;

/**
 * class for truncated kernel function
 *
 * @author Tamer Kulaksizoglu
 */
public class TruncatedKernelFunction extends AbstractKernelFunction {

    @Override
    public double valueAt(double z) {
        if (FastMath.abs(z) <= 1) {
            return 1;
        } else {
            return 0;
        }
    }
}
