package jecon.main.func.kernel;

import org.apache.commons.math3.util.FastMath;

/**
 * class for Epanechnikov kernel function
 *
 * @author Tamer Kulaksizoglu
 */
public class EpanechnikovKernelFunction extends AbstractKernelFunction {

    private static final double constant = (double) 3 / 4;

    @Override
    public double valueAt(double z) {
        if (FastMath.abs(z) <= 1) {
            return constant * (1 - FastMath.pow(z, 2));
        } else {
            return 0;
        }
    }
}
