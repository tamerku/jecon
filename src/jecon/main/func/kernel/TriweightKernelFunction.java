package jecon.main.func.kernel;

import org.apache.commons.math3.util.FastMath;

/**
 * class for triweight kernel function
 *
 * @author Tamer Kulaksizoglu
 */
public class TriweightKernelFunction extends AbstractKernelFunction {

    private static final double constant = (double) 35 / 32;

    @Override
    public double valueAt(double z) {
        if (FastMath.abs(z) <= 1) {
            return constant * FastMath.pow(1 - FastMath.pow(z, 2), 3);
        } else {
            return 0;
        }
    }
}
