package jecon.main.func.kernel;

import org.apache.commons.math3.util.FastMath;

/**
 * class for Gaussian kernel function
 *
 * @author Tamer Kulaksizoglu
 */
public class GaussianKernelFunction extends AbstractKernelFunction {

    private static final double constant = 1 / FastMath.sqrt(2 * FastMath.PI);

    @Override
    public double valueAt(double z) {
        return constant * FastMath.exp(-0.5 * FastMath.pow(z, 2));
    }
}
