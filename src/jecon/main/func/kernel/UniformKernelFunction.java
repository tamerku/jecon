package jecon.main.func.kernel;

import org.apache.commons.math3.util.FastMath;

/**
 * class for uniform kernel function
 *
 * @author Tamer Kulaksizoglu
 */
public class UniformKernelFunction extends AbstractKernelFunction {

    private static final double constant = (double) 1 / 2;

    @Override
    public double valueAt(double z) {
        if (FastMath.abs(z) <= 1) {
            return constant;
        } else {
            return 0;
        }
    }
}
