package jecon.main.func.kernel;

import org.apache.commons.math3.util.FastMath;

/**
 * class for cosine kernel function
 *
 * @author Tamer Kulaksizoglu
 */
public class CosineKernelFunction extends AbstractKernelFunction {

    private static final double pi2 = FastMath.PI / 2;
    private static final double pi4 = FastMath.PI / 4;

    @Override
    public double valueAt(double z) {
        if (FastMath.abs(z) <= 1) {
            return pi4 * FastMath.cos(pi2 * z);
        } else {
            return 0;
        }
    }
}
