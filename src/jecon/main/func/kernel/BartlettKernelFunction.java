package jecon.main.func.kernel;

import org.apache.commons.math3.util.FastMath;

/**
 * class for Bartlett kernel function
 *
 * @author Tamer Kulaksizoglu
 */
public class BartlettKernelFunction extends AbstractKernelFunction {

    @Override
    public double valueAt(double z) {
        if (FastMath.abs(z) <= 1) {
            return 1 - FastMath.abs(z);
        } else {
            return 0;
        }
    }
}
