package jecon.main.func.kernel;

import org.apache.commons.math3.util.FastMath;

/**
 * class for Parzen kernel function
 *
 * @author Tamer Kulaksizoglu
 */
public class ParzenKernelFunction extends AbstractKernelFunction {

    @Override
    public double valueAt(double z) {
        if (z >= 0 && z < 0.5) {
            return 1 - 6 * FastMath.pow(z, 2) + 6 * FastMath.pow(FastMath.abs(z), 3);
        } else if (z >= 0.5 && z <= 1) {
            return 2 * FastMath.pow(1 - FastMath.abs(z), 3);
        } else {
            return 0;
        }
    }
}
