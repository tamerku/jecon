package jecon.main.sim;

import org.apache.commons.math3.random.MersenneTwister;

/**
 * abstract base class for simulators
 *
 * @author Tamer Kulaksizoglu
 */
public abstract class AbstractSimulator {

    /**
     * counter for random numbers drawn
     */
    protected long counter;

    /**
     * generated data
     */
    protected double[] data;

    /**
     * number of discarded observations
     */
    protected int numOfDis;

    /**
     * number of effective observations
     */
    protected int numOfObs;

    /**
     * random number generator
     */
    protected MersenneTwister rng;

    /**
     * seed for random number generator
     */
    protected long seed;

    /**
     * gets count of random numbers drawn
     *
     * @return count of random numbers drawn
     */
    public long getCounter() {
        return counter;
    }

    /**
     * gets simulated data
     *
     * @return simulated data
     */
    public double[] getData() {
        return data;
    }

    /**
     * sets number of discarded observations
     *
     * @param numOfDis number of discarded observations
     */
    public void setNumberOfDiscards(int numOfDis) {
        this.numOfDis = numOfDis;
    }

    /**
     * gets number of discarded observations
     *
     * @return number of discarded observations
     */
    public int getNumberOfDiscards() {
        return numOfDis;
    }

    /**
     * sets number of effective observations
     *
     * @param numOfObs number of effective observations
     */
    public void setNumberOfObservations(int numOfObs) {
        this.numOfObs = numOfObs;
        data = new double[numOfObs];
    }

    /**
     * gets number of effective observations
     *
     * @return number of effective observations
     */
    public int getNumberOfObservations() {
        return numOfObs;
    }

    /**
     * sets seed
     *
     * @param seed seed
     */
    public void setSeed(long seed) {
        this.seed = seed;
        rng.setSeed(seed);
    }

    /**
     * gets seed
     *
     * @return seed
     */
    public long getSeed() {
        return seed;
    }

    /**
     * constructor for AbstractSimulator class
     */
    public AbstractSimulator() {
        counter = 0;
        data = null;
        numOfDis = 0;
        numOfObs = 100;
        rng = new MersenneTwister();
        seed = 1;
    }

    /**
     * simulates data
     */
    public abstract void simulate();
}
