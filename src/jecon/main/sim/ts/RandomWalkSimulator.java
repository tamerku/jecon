package jecon.main.sim.ts;

import org.apache.commons.math3.util.FastMath;
import jecon.main.sim.AbstractSimulator;

/**
 * class for simulating random walk
 *
 * @author Tamer Kulaksizoglu
 */
public class RandomWalkSimulator extends AbstractSimulator {

    private double drift;
    private double mean;
    private double stDev;

    /**
     * sets drift term
     *
     * @param drift drift term
     */
    public void setDrift(double drift) {
        this.drift = drift;
    }

    /**
     * gets drift term
     *
     * @return drift term
     */
    public double getDrift() {
        return drift;
    }

    /**
     * sets mean of error term
     *
     * @param mean mean of error term
     */
    public void setMean(double mean) {
        this.mean = mean;
    }

    /**
     * gets mean of error term
     *
     * @return mean of error term
     */
    public double getMean() {
        return mean;
    }

    /**
     * sets variance of error term
     *
     * @param variance variance of error term
     */
    public void setVariance(double variance) {
        stDev = FastMath.sqrt(variance);
    }

    /**
     * gets variance of error term
     *
     * @return variance of error term
     */
    public double getVariance() {
        return FastMath.pow(stDev, 2);
    }

    /**
     * constructor for <code>RandomWalkSimulator</code> class
     */
    public RandomWalkSimulator() {
        super();
        drift = 0;
        mean = 0;
        stDev = 1;
    }

    @Override
    public void simulate() {
        double e_t, y_t, y_t_1 = 0;
        for (int t = 0; t < numOfDis; t++) {
            e_t = mean + stDev * rng.nextGaussian();
            counter++;
            y_t = drift + y_t_1 + e_t;
            y_t_1 = y_t;
        }
        for (int t = 0; t < numOfObs; t++) {
            e_t = mean + stDev * rng.nextGaussian();
            counter++;
            y_t = drift + y_t_1 + e_t;
            data[t] = y_t;
            y_t_1 = y_t;
        }
    }
}
