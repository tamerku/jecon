package jecon.main.articles.lnv98;

import org.apache.commons.math3.analysis.differentiation.DerivativeStructure;
import org.apache.commons.math3.util.FastMath;

/**
 * class for logistic transition function
 *
 * @author Tamer Kulaksizoglu
 */
public class LogisticTransitionFunction {

    private final double[] derivatives;
    private DerivativeStructure dsGamma;
    private DerivativeStructure dsTau;
    private double gamma;
    private double[] gammaGrid;
    private int sampleSize;
    private double tau;
    private double[] tauGrid;

    /**
     * sets gamma parameter
     *
     * @param gamma smoothness parameter
     */
    public void setGamma(double gamma) {
        this.gamma = gamma;
        dsGamma = new DerivativeStructure(2, 1, 0, gamma);
    }

    /**
     * sets grid for gamma parameter
     *
     * @param gammaMin minimum gamma value
     * @param gammaMax maximum gamma value
     * @param gammaSteps steps in grid
     */
    public void setGammaGrid(double gammaMin, double gammaMax, int gammaSteps) {
        gammaGrid = new double[gammaSteps];
        if (gammaSteps == 1) {
            gammaGrid[0] = gammaMax;
        } else {
            double increment = (FastMath.log(gammaMax) - FastMath.log(gammaMin)) / (gammaSteps - 1);
            for (int i = 0; i < gammaSteps; i++) {
                gammaGrid[i] = FastMath.exp(gammaMin + i * increment);
            }
        }
    }

    /**
     * gets gamma parameter
     *
     * @return gamma parameter
     */
    public double getGamma() {
        return gamma;
    }

    /**
     * sets sample size
     *
     * @param sampleSize sample size
     */
    public void setSampleSize(int sampleSize) {
        this.sampleSize = sampleSize;
    }

    /**
     * gets sample size
     *
     * @return sample size
     */
    public int getSampleSize() {
        return sampleSize;
    }

    /**
     * sets tau parameter
     *
     * @param tau location parameter
     */
    public void setTau(double tau) {
        this.tau = tau;
        dsTau = new DerivativeStructure(2, 1, 1, tau);
    }

    /**
     * sets grid for tau parameter
     *
     * @param tauMin minimum tau value
     * @param tauMax minimum tau value
     * @param tauSteps steps in grid
     */
    public void setTauGrid(double tauMin, double tauMax, int tauSteps) {
        tauGrid = new double[tauSteps];
        if (tauSteps == 1) {
            tauGrid[0] = tauMax;
        } else {
            double increment = (tauMax - tauMin) / (tauSteps - 1);
            for (int i = 0; i < tauSteps; i++) {
                tauGrid[i] = tauMin + i * increment;
            }
        }
    }

    /**
     * gets tau parameter
     *
     * @return tau parameter
     */
    public double getTau() {
        return tau;
    }

    /**
     * class constructor
     */
    public LogisticTransitionFunction() {
        derivatives = new double[2];
        dsGamma = null;
        dsTau = null;
        gamma = Double.NaN;
        gammaGrid = null;
        sampleSize = 0;
        tau = Double.NaN;
        tauGrid = null;
    }

    /**
     * returns function derivatives with respect to gamma and tau at t
     *
     * @param t must go from 1 to sample size
     * @return function derivatives with respect to gamma and tau at t
     */
    public double[] derivativeAt(int t) {
        DerivativeStructure func = dsTau.multiply(sampleSize).negate().add((double) t);
        func = dsGamma.negate().multiply(func).exp().add(1).reciprocal();
        derivatives[0] = func.getPartialDerivative(1, 0);
        derivatives[1] = func.getPartialDerivative(0, 1);
        return derivatives;
    }

    /**
     * returns grid
     * <p>
     * <code>setGammaGrid</code> and <code>setTauGrid</code> must be called
     * before calling this method
     *
     * @return grid
     */
    public double[][] grid() {
        int n = gammaGrid.length * tauGrid.length;
        double[][] grid = new double[n][2];
        int counter = -1;
        for (int g = 0; g < gammaGrid.length; g++) {
            for (int t = 0; t < tauGrid.length; t++) {
                counter++;
                grid[counter][0] = gammaGrid[g];
                grid[counter][1] = tauGrid[t];
            }
        }
        return grid;
    }

    /**
     * returns function value at t
     *
     * @param t must go from 1 to sample size
     * @return function value at t
     */
    public double valueAt(int t) {
        return FastMath.pow(1 + FastMath.exp(-gamma * (t - tau * sampleSize)), -1);
    }

}
