package jecon.main.articles.lnv98;

import org.apache.commons.math3.analysis.MultivariateMatrixFunction;
import org.apache.commons.math3.analysis.MultivariateVectorFunction;
import org.apache.commons.math3.exception.ConvergenceException;
import org.apache.commons.math3.fitting.leastsquares.EvaluationRmsChecker;
import org.apache.commons.math3.fitting.leastsquares.LeastSquaresBuilder;
import org.apache.commons.math3.fitting.leastsquares.LeastSquaresOptimizer;
import org.apache.commons.math3.fitting.leastsquares.LeastSquaresProblem;
import org.apache.commons.math3.fitting.leastsquares.LevenbergMarquardtOptimizer;
import org.apache.commons.math3.linear.RealVector;
import org.apache.commons.math3.optim.ConvergenceChecker;
import org.apache.commons.math3.util.FastMath;

/**
 * abstract base class for models A, B, and C
 *
 * @author Tamer Kulaksizoglu
 */
public abstract class AbstractModel {

    protected ConvergenceChecker<LeastSquaresProblem.Evaluation> checker;
    protected int coefCount; // value set by concrete classes
    protected double[][] jacobian;
    protected LevenbergMarquardtOptimizer lmo;
    protected LeastSquaresBuilder lsb;
    protected LeastSquaresProblem lsp;
    protected LogisticTransitionFunction ltf;
    protected LeastSquaresOptimizer.Optimum opt;
    protected double[] par0;
    protected RealVector par;
    protected double[] residuals;
    protected int sampleSize;
    protected double[] y;
    protected double[] yhat;

    /**
     * sets response variable
     *
     * @param y response variable
     */
    public void setResponseVariable(double[] y) {
        this.y = y;
        lsb.target(y);
        sampleSize = y.length;
        jacobian = new double[sampleSize][coefCount + 2];
        ltf.setSampleSize(sampleSize);
        yhat = new double[sampleSize];
        residuals = new double[sampleSize];
    }

    /**
     * gets estimated parameters
     *
     * @return estimated parameters
     */
    public RealVector getPar() {
        return par;
    }

    /**
     * class constructor
     *
     * @param tolerance absolute and relative tolerance
     */
    public AbstractModel(double tolerance) {
        checker = new EvaluationRmsChecker(tolerance);
        lmo = new LevenbergMarquardtOptimizer();
        lsb = new LeastSquaresBuilder();
        lsb.checker(checker);
        lsb.maxEvaluations(5000);
        lsb.maxIterations(1000);
        lsp = null;
        ltf = new LogisticTransitionFunction();
        ltf.setGammaGrid(0.1, 5, 30);
        ltf.setTauGrid(0.01, 0.99, 30);
        opt = null;
        par0 = null;
        par = null;
        residuals = null;
        sampleSize = 0;
        y = null;
        yhat = null;
    }

    /**
     * calculates and returns residuals
     *
     * @return residuals
     */
    public double[] calculateResiduals() {
        for (int t = 0; t < sampleSize; t++) {
            residuals[t] = y[t] - yhat[t];
        }
        return residuals;
    }

    /**
     * calculates and returns residual sum of squares
     *
     * @return residual sum of squares
     */
    public double calculateRSS() {
        this.calculateResiduals();
        double rss = 0;
        for (int t = 0; t < sampleSize; t++) {
            rss += FastMath.pow(residuals[t], 2);
        }
        return rss;
    }

    /**
     * estimates model parameters
     *
     * @param start stating values (if null, grid search is used)
     */
    public boolean estimate(double[] start) {
        lsb.model(this.getModelFunction(), this.getModelFunctionJacobian());
        if (start != null) {
            lsb.start(start);
        } else {
            par0 = this.getStartingValues();
            lsb.start(par0);
        }
        lsp = lsb.build();
        try {
            opt = lmo.optimize(lsp);
            par = opt.getPoint();
            return true;
        } catch (ConvergenceException e) {
            return false;
        }
    }

    /**
     * gets fitted values
     *
     * @return fitted values
     */
    public double[] getFitted() {
        return yhat;
    }

    /**
     * sets gamma grid
     *
     * @param gammaMin minimum gamma value
     * @param gammaMax maximum gamma value
     * @param gammaSteps steps in grid
     */
    public void setGammaGrid(double gammaMin, double gammaMax, int gammaSteps) {
        ltf.setGammaGrid(gammaMin, gammaMax, gammaSteps);
    }

    /**
     * sets tau grid
     *
     * @param tauMin minimum tau value
     * @param tauMax maximum tau value
     * @param tauSteps steps in grid
     */
    public void setTauGrid(double tauMin, double tauMax, int tauSteps) {
        ltf.setTauGrid(tauMin, tauMax, tauSteps);
    }

    /**
     * gets model function
     *
     * @return model function
     */
    public abstract MultivariateVectorFunction getModelFunction();

    /**
     * gets jacobian of model function
     *
     * @return jacobian of model function
     */
    public abstract MultivariateMatrixFunction getModelFunctionJacobian();

    public abstract double[] getStartingValues();

}
