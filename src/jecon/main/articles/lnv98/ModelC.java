package jecon.main.articles.lnv98;

import org.apache.commons.math3.analysis.MultivariateMatrixFunction;
import org.apache.commons.math3.analysis.MultivariateVectorFunction;
import org.apache.commons.math3.stat.regression.OLSMultipleLinearRegression;

/**
 * class for ModelC
 *
 * @author Tamer Kulaksizoglu
 */
public class ModelC extends AbstractModel {

    /**
     * class constructor
     *
     * @param tolerance absolute and relative tolerance
     */
    public ModelC(double tolerance) {
        super(tolerance);
        coefCount = 4; // a1, a2, b1, b2
        par0 = new double[coefCount + 2]; // a1, a2, b1, b2, gamma, tau
    }

    @Override
    public MultivariateVectorFunction getModelFunction() {
        return (double[] params) -> {
            double a1 = params[0];
            double a2 = params[1];
            double b1 = params[2];
            double b2 = params[3];
            double gamma = params[4];
            double tau = params[5];
            ltf.setGamma(gamma);
            ltf.setTau(tau);
            for (int t = 0; t < sampleSize; t++) {
                yhat[t] = a1 + a2 * ltf.valueAt(t + 1) + b1 * (t + 1)
                        + b2 * (t + 1) * ltf.valueAt(t + 1);
            }
            return yhat;
        };
    }

    @Override
    public MultivariateMatrixFunction getModelFunctionJacobian() {
        return (double[] params) -> {
            double a1 = params[0];
            double a2 = params[1];
            double b1 = params[2];
            double b2 = params[3];
            double gamma = params[4];
            double tau = params[5];
            ltf.setGamma(gamma);
            ltf.setTau(tau);
            double[] der;
            for (int t = 0; t < sampleSize; t++) {
                jacobian[t][0] = 1;
                jacobian[t][1] = ltf.valueAt(t + 1);
                jacobian[t][2] = t + 1;
                jacobian[t][3] = ltf.valueAt(t + 1) * (t + 1);
                der = ltf.derivativeAt(t + 1);
                jacobian[t][4] = (a2 + b2 * (t + 1)) * der[0];
                jacobian[t][5] = (a2 + b2 * (t + 1)) * der[1];
            }
            return jacobian;
        };
    }

    @Override
    public double[] getStartingValues() {
        double[][] grid = ltf.grid();
        double rss, rssmin = Double.MAX_VALUE;
        int imin = 0;
        OLSMultipleLinearRegression sr = new OLSMultipleLinearRegression();
        double[][] X = new double[sampleSize][coefCount];
        sr.setNoIntercept(true);
        for (int i = 0; i < grid.length; i++) {
            ltf.setGamma(grid[i][0]);
            ltf.setTau(grid[i][1]);
            for (int t = 0; t < sampleSize; t++) {
                X[t][0] = 1;
                X[t][1] = ltf.valueAt(t + 1);
                X[t][2] = t + 1;
                X[t][3] = ltf.valueAt(t + 1) * (t + 1);
            }
            sr.newSampleData(y, X);
            rss = sr.calculateResidualSumOfSquares();
            if (rss < rssmin) {
                rssmin = rss;
                imin = i;
            }
        }
        double gamma = grid[imin][0];
        double tau = grid[imin][1];
        ltf.setGamma(gamma);
        ltf.setTau(tau);
        for (int t = 0; t < sampleSize; t++) {
            X[t][0] = 1;
            X[t][1] = ltf.valueAt(t + 1);
            X[t][2] = t + 1;
            X[t][3] = ltf.valueAt(t + 1) * (t + 1);
        }
        sr.newSampleData(y, X);
        double[] coef = sr.estimateRegressionParameters();
        double a1 = coef[0];
        double a2 = coef[1];
        double b1 = coef[2];
        double b2 = coef[3];
        par0[0] = a1;
        par0[1] = a2;
        par0[2] = b1;
        par0[3] = b2;
        par0[4] = gamma;
        par0[5] = tau;
        return par0;
    }
}
