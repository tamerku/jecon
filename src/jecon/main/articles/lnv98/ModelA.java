package jecon.main.articles.lnv98;

import org.apache.commons.math3.analysis.MultivariateMatrixFunction;
import org.apache.commons.math3.analysis.MultivariateVectorFunction;
import org.apache.commons.math3.stat.regression.SimpleRegression;

/**
 * class for ModelA
 *
 * @author Tamer Kulaksizoglu
 */
public class ModelA extends AbstractModel {

    /**
     * class constructor
     *
     * @param tolerance absolute and relative tolerance
     */
    public ModelA(double tolerance) {
        super(tolerance);
        coefCount = 2; // a1, a2
        par0 = new double[coefCount + 2]; // a1, a2, gamma, tau
        //riso.numerical.LBFG
    }

    @Override
    public MultivariateVectorFunction getModelFunction() {
        return (double[] params) -> {
            double a1 = params[0];
            double a2 = params[1];
            double gamma = params[2];
            double tau = params[3];
            ltf.setGamma(gamma);
            ltf.setTau(tau);
            for (int t = 0; t < sampleSize; t++) {
                yhat[t] = a1 + a2 * ltf.valueAt(t + 1);
            }
            return yhat;
        };
    }

    @Override
    public MultivariateMatrixFunction getModelFunctionJacobian() {
        return (double[] params) -> {
            double a1 = params[0];
            double a2 = params[1];
            double gamma = params[2];
            double tau = params[3];
            ltf.setGamma(gamma);
            ltf.setTau(tau);
            double[] der;
            for (int t = 0; t < sampleSize; t++) {
                jacobian[t][0] = 1;
                jacobian[t][1] = ltf.valueAt(t + 1);
                der = ltf.derivativeAt(t + 1);
                jacobian[t][2] = a2 * der[0];
                jacobian[t][3] = a2 * der[1];
            }
            return jacobian;
        };
    }

    @Override
    public double[] getStartingValues() {
        double[][] grid = ltf.grid();
        double rss, rssmin = Double.MAX_VALUE;
        int imin = 0;
        SimpleRegression sr = new SimpleRegression();
        for (int i = 0; i < grid.length; i++) {
            ltf.setGamma(grid[i][0]);
            ltf.setTau(grid[i][1]);
            sr.clear();
            for (int t = 0; t < sampleSize; t++) {
                sr.addData(ltf.valueAt(t + 1), y[t]);
            }
            sr.regress();
            rss = sr.getSumSquaredErrors();
            if (rss < rssmin) {
                rssmin = rss;
                imin = i;
            }
        }
        double gamma = grid[imin][0];
        double tau = grid[imin][1];
        ltf.setGamma(gamma);
        ltf.setTau(tau);
        sr.clear();
        for (int t = 0; t < sampleSize; t++) {
            sr.addData(ltf.valueAt(t + 1), y[t]);
        }
        sr.regress();
        double a1 = sr.getIntercept();
        double a2 = sr.getSlope();
        par0[0] = a1;
        par0[1] = a2;
        par0[2] = gamma;
        par0[3] = tau;
        return par0;
    }

}
