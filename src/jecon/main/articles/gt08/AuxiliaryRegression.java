package jecon.main.articles.gt08;

import jecon.main.est.Estimator;
import org.apache.commons.math3.stat.regression.OLSMultipleLinearRegression;

/**
 *
 * @author Tamer Kulaksizoglu
 */
public class AuxiliaryRegression implements Estimator {

    private OLSMultipleLinearRegression[][] estimators;
    private EstimationOrder order;
    private int P;
    private int Q;
    private double[] y;

    public OLSMultipleLinearRegression getEstimator(int p, int q) {
        return estimators[p][q];
    }

    public void setOrder(EstimationOrder order) {
        this.order = order;
    }

    public void setPQ(int P, int Q) {
        this.P = P;
        this.Q = Q;
        estimators = new OLSMultipleLinearRegression[P + 1][Q + 1];
        for (int q = 0; q < Q + 1; q++) {
            estimators[q] = new OLSMultipleLinearRegression[P + 1];
            for (int p = 0; p < P + 1; p++) {
                estimators[q][p] = new OLSMultipleLinearRegression();
                estimators[q][p].setNoIntercept(true);
            }
        }
    }

    public void setY(double[] y) {
        this.y = y;
    }

    public AuxiliaryRegression() {
        estimators = null;
        order = EstimationOrder.QP;
        P = 0;
        Q = 0;
        y = null;
    }

    @Override
    public void estimate() {
        if (P == 0 && Q == 0) {
            int n = y.length;
            double[][] X = new double[n][1];
            for (int i = 0; i < n; i++) {
                X[i][0] = 1;
            }
            estimators[0][0].newSampleData(y, X);
        } else if (P == 0 && Q > 0) {

        } else if (P > 0 && Q == 0) {

        } else if (P > 0 && Q > 0) {

        } else {

        }
    }
}
