package jecon.main.articles.gt08;

/**
 * enumeration for estimation order
 *
 * @author Tamer Kulaksizoglu
 */
public enum EstimationOrder {

    PQ,
    QP
}
