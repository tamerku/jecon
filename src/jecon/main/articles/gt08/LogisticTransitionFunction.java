package jecon.main.articles.gt08;

import org.apache.commons.math3.stat.descriptive.SummaryStatistics;
import org.apache.commons.math3.util.FastMath;

/**
 * class for logistic transition function
 *
 * @author Tamer Kulaksizoglu
 */
public class LogisticTransitionFunction {

    private double c;
    private double gamma;
    private int sampleSize;
    private double stdDev;
    private final SummaryStatistics summaryStatistics;

    public void setLocationParameter(double c) {
        this.c = c;
    }

    public double getLocationParameter() {
        return c;
    }

    public void setSampleSize(int sampleSize) {
        this.sampleSize = sampleSize;
        summaryStatistics.clear();
        for (int t = 1; t <= sampleSize; t++) {
            summaryStatistics.addValue((double) t / sampleSize);
        }
        stdDev = summaryStatistics.getStandardDeviation();
    }

    public int getSampleSize() {
        return sampleSize;
    }

    public void setSmoothnessParameter(double gamma) {
        this.gamma = gamma;
    }

    public double getSmoothnessParameter() {
        return gamma;
    }

    public double getStandardDeviation() {
        return stdDev;
    }

    public LogisticTransitionFunction() {
        c = Double.NaN;
        gamma = Double.NaN;
        sampleSize = 0;
        stdDev = Double.NaN;
        summaryStatistics = new SummaryStatistics();
    }

    public double valueAt(int t) {
        return FastMath.pow(1 + FastMath.exp(-gamma * ((double) t / sampleSize - c) / stdDev), -1);
    }
}
