package jecon.main.articles.gt08;

public class Program1 {

    public static void main(String[] args) {
        double[] y = new double[]{0.2249, 1.7400, -0.2043, -0.9176, -0.6742,
            -0.3435, 0.2233, -0.1414, -0.1834, 0.6804, 0.0906, -0.8333, 0.8135,
            1.1174, 0.3150, -0.5003, -1.6268, 0.6194, -1.4574, -1.8035};
        AuxiliaryRegression reg = new AuxiliaryRegression();
        reg.setOrder(EstimationOrder.PQ);
        reg.setPQ(0, 0);
        reg.setY(y);
        reg.estimate();
    }
}
