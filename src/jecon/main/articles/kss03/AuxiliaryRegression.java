package jecon.main.articles.kss03;

import jecon.main.est.Estimator;
import org.apache.commons.math3.stat.regression.OLSMultipleLinearRegression;
import org.apache.commons.math3.util.FastMath;
import jecon.main.util.DataTransformer;

/**
 * class to estimate auxiliary regression
 *
 * @author Tamer Kulaksizoglu
 */
public class AuxiliaryRegression implements Estimator {

    private int lag;
    private int maximumLag;
    private final OLSMultipleLinearRegression ols;
    private double[] series;
    private double[] transformedSeries;
    private Transformation transformation;

    /**
     * sets lag
     *
     * @param lag
     * @param maximumLag
     */
    public void setLagStructure(int lag, int maximumLag) {
        this.lag = lag;
        this.maximumLag = maximumLag;
    }

    /**
     * gets lag
     *
     * @return lag
     */
    public int getLag() {
        return lag;
    }

    public int getMaximumLag() {
        return maximumLag;
    }

    /**
     * sets series
     *
     * @param series
     */
    public void setSeries(double[] series) {
        this.series = series;
        transformedSeries = new double[series.length];
    }

    /**
     * gets series
     *
     * @return series
     */
    public double[] getSeries() {
        return series;
    }

    /**
     * sets transformation
     *
     * @param transformation
     */
    public void setTransformation(Transformation transformation) {
        this.transformation = transformation;
    }

    /**
     * gets transformation
     *
     * @return transformation
     */
    public Transformation getTransformation() {
        return transformation;
    }

    /**
     * class constructor
     */
    public AuxiliaryRegression() {
        lag = 0;
        ols = new OLSMultipleLinearRegression();
        ols.setNoIntercept(true);
        series = null;
        transformedSeries = null;
        transformation = Transformation.NONE;
    }

    /**
     * estimates auxiliary regression
     */
    @Override
    public void estimate() {
        if (transformation == Transformation.NONE) {
            int n = series.length;
            System.arraycopy(series, 0, transformedSeries, 0, n);
        } else if (transformation == Transformation.DEMEAN) {
            DataTransformer.demean(series, transformedSeries);
        } else if (transformation == Transformation.DETREND) {
            transformedSeries = DataTransformer.detrend(series);
        }
        if (lag == 0) {
            double[] ly = DataTransformer.lagTrim(transformedSeries, 1);
            double[] dy = DataTransformer.diffTrim(transformedSeries);
            int n = ly.length;
            double[][] x = new double[n][1];
            for (int i = 0; i < n; i++) {
                x[i] = new double[]{FastMath.pow(ly[i], 3)}; // no constant
            }
            ols.newSampleData(dy, x);
        } else { // lag >= 1
            double[] ly = DataTransformer.lag(transformedSeries, 1);
            double[] dy = DataTransformer.diff(transformedSeries);
            int n = ly.length;
            int N = n - lag - 1;
            double[][] x = new double[N][lag + 1];
            double[] y = new double[N];
            for (int i = lag + 1; i < n; i++) {
                y[i - lag - 1] = dy[i];
                x[i - lag - 1][0] = FastMath.pow(ly[i], 3);
                for (int l = 1; l <= lag; l++) {
                    x[i - lag - 1][l] = dy[i - l];
                }
            }
            ols.newSampleData(y, x);
        }
    }

    /**
     * gets t-ratio
     *
     * @return t-ratio
     */
    public double getTStat() {
        double[] coef = ols.estimateRegressionParameters();
        double[] coefse = ols.estimateRegressionParametersStandardErrors();
        double coeft = coef[0] / coefse[0];
        return coeft;
    }
}
