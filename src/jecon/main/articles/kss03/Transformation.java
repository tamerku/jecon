package jecon.main.articles.kss03;

/**
 * enumeration for transformation types
 *
 * @author Tamer Kulaksizoglu
 */
public enum Transformation {

    DEMEAN,
    DETREND,
    NONE
}
