package jecon.main.est;

/**
 * interface for estimators
 *
 * @author Tamer Kulaksizoglu
 */
public interface Estimator {

    public void estimate();
}
