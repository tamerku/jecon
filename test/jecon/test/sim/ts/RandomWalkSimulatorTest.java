package jecon.test.sim.ts;

import org.junit.Test;
import static org.junit.Assert.*;
import jecon.main.sim.ts.RandomWalkSimulator;

/**
 * unit test for <code>RandomWalkSimulator</code> class
 *
 * @author Tamer Kulaksizoglu
 */
public class RandomWalkSimulatorTest {

    @Test
    public void testCase01() {
        RandomWalkSimulator rw = new RandomWalkSimulator();
        rw.setNumberOfDiscards(0);
        rw.setNumberOfObservations(10);
        rw.setSeed(1);
        rw.simulate();
        double[] actual = rw.getData();
        double[] expected = new double[]{1.001920383687780, 1.863037216877810,
            2.849041270840390, 2.952735231045720, 3.461778632742040, 2.065690454599910,
            0.856553800078958, 0.500252305644768, 0.909569005742667, 1.392398243084490};
        assertArrayEquals(expected, actual, 1e-14);
    }

    @Test
    public void testCase02() {
        RandomWalkSimulator rw = new RandomWalkSimulator();
        rw.setNumberOfDiscards(0);
        rw.setNumberOfObservations(10);
        rw.setSeed(2);
        rw.simulate();
        double[] actual = rw.getData();
        double[] expected = new double[]{0.866531296165219, 0.928985590648051,
            1.271357691607600, 1.278432773318570, 2.698401713901760, 1.806208230551250,
            0.826224525654657, 1.587180466088490, 1.044769515444890, 1.086062603320710};
        assertArrayEquals(expected, actual, 1e-14);
    }

    @Test
    public void testCase03() {
        RandomWalkSimulator rw = new RandomWalkSimulator();
        rw.setNumberOfDiscards(5);
        rw.setNumberOfObservations(10);
        rw.setSeed(3);
        rw.simulate();
        double[] actual = rw.getData();
        double[] expected = new double[]{0.543617442843052, -3.022023479907200,
            -3.364446380791800, -2.309711886985860, -2.722840297818970, -1.329988277377690,
            -0.176723096402279, 0.546615023535707, 2.194326460251800, 1.295456636393930};
        assertArrayEquals(expected, actual, 1e-13);
    }

    @Test
    public void testCase04() {
        RandomWalkSimulator rw = new RandomWalkSimulator();
        rw.setNumberOfDiscards(0);
        rw.setNumberOfObservations(10);
        rw.setSeed(1);
        rw.setDrift(0.5);
        rw.simulate();
        double[] actual = rw.getData();
        double[] expected = new double[]{1.50192038368778, 2.86303721687781,
            4.34904127084039, 4.95273523104572, 5.96177863274204, 5.06569045459991,
            4.35655380007896, 4.50025230564477, 5.40956900574267, 6.39239824308449};
        assertArrayEquals(expected, actual, 1e-14);
    }

    @Test
    public void testCase05() {
        RandomWalkSimulator rw = new RandomWalkSimulator();
        rw.setNumberOfDiscards(0);
        rw.setNumberOfObservations(10);
        rw.setSeed(1);
        rw.setDrift(0.95);
        rw.simulate();
        double[] actual = rw.getData();
        double[] expected = new double[]{1.95192038368778, 3.76303721687781,
            5.69904127084039, 6.75273523104572, 8.21177863274204, 7.76569045459991,
            7.50655380007896, 8.10025230564477, 9.45956900574267, 10.89239824308450};
        assertArrayEquals(expected, actual, 1e-13);
    }
}
