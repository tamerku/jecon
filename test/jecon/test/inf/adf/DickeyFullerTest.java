package jecon.test.inf.adf;

import org.junit.Test;
import static org.junit.Assert.*;
import jecon.main.inf.adf.Deterministic;
import jecon.main.inf.adf.DickeyFuller;
import jecon.main.inf.adf.Difference;
import jecon.main.inf.adf.InformationCriteria;

/**
 * unit test for <code>DickeyFuller</code> class
 *
 * @author Tamer
 */
public class DickeyFullerTest {

    @Test
    public void testCase01() {
        double[] series = new double[]{-0.062680699090387, 0.400542667660160,
            -0.718787981053512, 0.761355647624008, 0.158482971052969,
            0.004704836649814, -3.565640922750250, -0.342422900884603,
            1.054734493805940, -0.413128410833109, 1.392852020441280,
            1.153265180975410, 0.723338119937986, 1.647711436716090,
            -0.898869823857870};
        DickeyFuller df = new DickeyFuller();
        df.setDeterministic(Deterministic.NONE);
        df.setDifference(Difference.LEVEL);
        df.setSeries(series);
        double[] actual, expected;
        df.run();
        expected = new double[]{-2.74061340265981, -1.968429524643545, -1.604391945651989};
        actual = df.getCriticalValues();
        assertArrayEquals(expected, actual, 1e-3);
    }

    @Test
    public void testCase02() {
        double[] series = new double[]{-0.062680699090387, 0.400542667660160,
            -0.718787981053512, 0.761355647624008, 0.158482971052969,
            0.004704836649814, -3.565640922750250, -0.342422900884603,
            1.054734493805940, -0.413128410833109, 1.392852020441280,
            1.153265180975410, 0.723338119937986, 1.647711436716090,
            -0.898869823857870};
        DickeyFuller df = new DickeyFuller();
        df.setDeterministic(Deterministic.INTERCEPT);
        df.setDifference(Difference.LEVEL);
        df.setSeries(series);
        double[] actual, expected;
        df.run();
        expected = new double[]{-4.004424924017169, -3.098896405323372, -2.69043949557234};
        actual = df.getCriticalValues();
        assertArrayEquals(expected, actual, 1e-3);
    }

    @Test
    public void testCase03() {
        double[] series = new double[]{-0.062680699090387, 0.400542667660160,
            -0.718787981053512, 0.761355647624008, 0.158482971052969,
            0.004704836649814, -3.565640922750250, -0.342422900884603,
            1.054734493805940, -0.413128410833109, 1.392852020441280,
            1.153265180975410, 0.723338119937986, 1.647711436716090,
            -0.898869823857870};
        DickeyFuller df = new DickeyFuller();
        df.setDeterministic(Deterministic.TREND_AND_INTERCEPT);
        df.setDifference(Difference.LEVEL);
        df.setSeries(series);
        double[] actual, expected;
        df.run();
        expected = new double[]{-4.800079600754167, -3.791171736521128, -3.342252758603569};
        actual = df.getCriticalValues();
        assertArrayEquals(expected, actual, 1e-3);
    }

    @Test
    public void testCase04() {
        double[] series = new double[]{-0.062680699090387, 0.400542667660160,
            -0.718787981053512, 0.761355647624008, 0.158482971052969,
            0.004704836649814, -3.565640922750250, -0.342422900884603,
            1.054734493805940, -0.413128410833109, 1.392852020441280,
            1.153265180975410, 0.723338119937986, 1.647711436716090,
            -0.898869823857870};
        DickeyFuller df = new DickeyFuller();
        df.setDeterministic(Deterministic.TREND_AND_INTERCEPT);
        df.setDifference(Difference.LEVEL);
        df.setInformationCriteria(InformationCriteria.SCHWARZ);
        df.setLags(0, 4);
        df.setSeries(series);
        int actual, expected;
        df.run();
        expected = 0;
        actual = df.getSelectedlag();
        assertEquals(expected, actual);
    }

    @Test
    public void testCase05() {
        double[] series = new double[]{8.192349099999990, 8.191075200000000, 8.201769900000000,
            8.194753999999990, 8.218247899999990, 8.221505799999990, 8.225824299999990, 8.229137700000000,
            8.247743899999990, 8.267680000000000, 8.277488299999990, 8.294874300000000, 8.317155599999990,
            8.322515599999990, 8.324457600000000, 8.330188700000000, 8.325839000000000, 8.329609800000000,
            8.319473699999990, 8.315174900000000, 8.298365300000000, 8.308125099999990, 8.323826900000000,
            8.337683699999990, 8.356507900000000, 8.362385599999990, 8.366835300000000, 8.377195199999990,
            8.391130999999990, 8.406216900000000, 8.419977199999990, 8.418344799999990, 8.425910899999990,
            8.454210800000000, 8.462567999999990, 8.474849199999990, 8.477162599999990, 8.479034300000000,
            8.485764400000000, 8.488937699999990, 8.493166900000000, 8.467519599999990, 8.466993999999990,
            8.482746699999990, 8.495601799999990, 8.489801399999990, 8.496990500000000, 8.485413499999990,
            8.468633000000000, 8.475120399999990, 8.470960800000000, 8.473868100000000, 8.479242100000000,
            8.500921500000000, 8.516813100000000, 8.533302700000000, 8.549950799999990, 8.561152699999990,
            8.567163600000000, 8.573516800000000, 8.580487200000000, 8.587931199999990, 8.601019499999990,
            8.606832799999990, 8.617906899999990, 8.617092800000000, 8.622525700000000, 8.626585600000000,
            8.631699799999990, 8.641709099999990, 8.649308599999990, 8.663421100000000, 8.670412000000000,
            8.679090900000000, 8.684570300000000, 8.696577100000000, 8.705397400000000, 8.711459800000000,
            8.717469299999990, 8.719627799999990, 8.729413299999990, 8.732078700000000, 8.727259600000000,
            8.719676800000000, 8.712381600000000, 8.714370499999990, 8.716306299999990, 8.719366400000000,
            8.731029599999990, 8.736216300000000, 8.743563500000000, 8.753923999999990, 8.756524999999990,
            8.760468700000000, 8.767079900000000, 8.778926500000000, 8.787037099999990, 8.797427600000000,
            8.802131499999990, 8.811637299999990, 8.816111899999990, 8.818571000000000, 8.825089600000000,
            8.833725100000000, 8.840203799999990, 8.855862999999990, 8.859377699999990, 8.871421099999990,
            8.883765000000000, 8.894780000000000, 8.905905400000000, 8.910000000000000};
        DickeyFuller df = new DickeyFuller();
        df.setDeterministic(Deterministic.INTERCEPT);
        df.setDifference(Difference.LEVEL);
        df.setInformationCriteria(InformationCriteria.AKAIKE);
        df.setLags(0, 12);
        df.setSeries(series);
        int actual, expected;
        df.run();
        expected = 12;
        actual = df.getSelectedlag();
        assertEquals(expected, actual);
        df.setInformationCriteria(InformationCriteria.SCHWARZ);
        df.run();
        expected = 1;
        actual = df.getSelectedlag();
        assertEquals(expected, actual);
    }
}
