package jecon.test.inf.adf;

import org.junit.Test;
import static org.junit.Assert.*;
import jecon.main.inf.adf.AuxiliaryRegression;
import jecon.main.inf.adf.Deterministic;
import jecon.main.inf.adf.Difference;
import jecon.main.inf.adf.InformationCriteria;

/**
 * unit test for <code>AuxiliaryRegression</code> class
 *
 * @author Tamer Kulaksizoglu
 */
public class AuxiliaryRegressionTest {

    @Test
    public void testCase01() {
        double[] series = new double[]{-0.062680699090387, 0.400542667660160,
            -0.718787981053512, 0.761355647624008, 0.158482971052969,
            0.004704836649814, -3.565640922750250, -0.342422900884603,
            1.054734493805940, -0.413128410833109, 1.392852020441280,
            1.153265180975410, 0.723338119937986, 1.647711436716090,
            -0.898869823857870};
        AuxiliaryRegression areg = new AuxiliaryRegression();
        areg.setSeries(series);
        areg.setDiffference(Difference.LEVEL);
        double actual, expected;
        areg.setDeterministic(Deterministic.NONE);
        areg.estimate();
        actual = areg.getTestStat();
        expected = -3.34524211207404;
        assertEquals(expected, actual, 1e-15);
        areg.setDeterministic(Deterministic.INTERCEPT);
        areg.estimate();
        actual = areg.getTestStat();
        expected = -3.226864501667266;
        assertEquals(expected, actual, 1e-15);
        areg.setDeterministic(Deterministic.TREND_AND_INTERCEPT);
        areg.estimate();
        actual = areg.getTestStat();
        expected = -3.249920736852058;
        assertEquals(expected, actual, 1e-15);
    }

    @Test
    public void testCase02() {
        double[] series = new double[]{-0.062680699090387, 0.400542667660160,
            -0.718787981053512, 0.761355647624008, 0.158482971052969,
            0.004704836649814, -3.565640922750250, -0.342422900884603,
            1.054734493805940, -0.413128410833109, 1.392852020441280,
            1.153265180975410, 0.723338119937986, 1.647711436716090,
            -0.898869823857870};
        AuxiliaryRegression areg = new AuxiliaryRegression();
        areg.setSeries(series);
        areg.setDiffference(Difference.FIRST_DIFFERENCE);
        double actual, expected;
        areg.setDeterministic(Deterministic.NONE);
        areg.estimate();
        actual = areg.getTestStat();
        expected = -5.2906070219911;
        assertEquals(expected, actual, 1e-15);
        areg.setDeterministic(Deterministic.INTERCEPT);
        areg.estimate();
        actual = areg.getTestStat();
        expected = -5.044671042976581;
        assertEquals(expected, actual, 1e-15);
        areg.setDeterministic(Deterministic.TREND_AND_INTERCEPT);
        areg.estimate();
        actual = areg.getTestStat();
        expected = -4.77784026577559;
        assertEquals(expected, actual, 1e-15);
    }

    @Test
    public void testCase03() {
        double[] series = new double[]{-0.062680699090387, 0.400542667660160,
            -0.718787981053512, 0.761355647624008, 0.158482971052969,
            0.004704836649814, -3.565640922750250, -0.342422900884603,
            1.054734493805940, -0.413128410833109, 1.392852020441280,
            1.153265180975410, 0.723338119937986, 1.647711436716090,
            -0.898869823857870};
        AuxiliaryRegression areg = new AuxiliaryRegression();
        areg.setSeries(series);
        areg.setDiffference(Difference.SECOND_DIFFERENCE);
        double actual, expected;
        areg.setDeterministic(Deterministic.NONE);
        areg.estimate();
        actual = areg.getTestStat();
        expected = -6.474358660039148;
        assertEquals(expected, actual, 1e-14);
        areg.setDeterministic(Deterministic.INTERCEPT);
        areg.estimate();
        actual = areg.getTestStat();
        expected = -6.17562502426498;
        assertEquals(expected, actual, 1e-14);
        areg.setDeterministic(Deterministic.TREND_AND_INTERCEPT);
        areg.estimate();
        actual = areg.getTestStat();
        expected = -5.911239132592099;
        assertEquals(expected, actual, 1e-14);
    }

    @Test
    public void testCase04() {
        double[] series = new double[]{-0.062680699090387, 0.400542667660160,
            -0.718787981053512, 0.761355647624008, 0.158482971052969,
            0.004704836649814, -3.565640922750250, -0.342422900884603,
            1.054734493805940, -0.413128410833109, 1.392852020441280,
            1.153265180975410, 0.723338119937986, 1.647711436716090,
            -0.898869823857870};
        AuxiliaryRegression areg = new AuxiliaryRegression();
        areg.setDeterministic(Deterministic.NONE);
        areg.setDiffference(Difference.LEVEL);
        areg.setSeries(series);
        double actual, expected;
        areg.setInformationCriteria(InformationCriteria.AKAIKE);
        areg.estimate();
        actual = areg.calculateInformationCriteria();
        expected = 3.461466035798979;
        assertEquals(expected, actual, 1e-15);
        areg.setInformationCriteria(InformationCriteria.HANNAN_QUINN);
        areg.estimate();
        actual = areg.calculateInformationCriteria();
        expected = 3.45724057598146;
        assertEquals(expected, actual, 1e-15);
        areg.setInformationCriteria(InformationCriteria.SCHWARZ);
        areg.estimate();
        actual = areg.calculateInformationCriteria();
        expected = 3.507112987914354;
        assertEquals(expected, actual, 1e-15);
    }

    @Test
    public void testCase05() {
        double[] series = new double[]{-0.062680699090387, 0.400542667660160,
            -0.718787981053512, 0.761355647624008, 0.158482971052969,
            0.004704836649814, -3.565640922750250, -0.342422900884603,
            1.054734493805940, -0.413128410833109, 1.392852020441280,
            1.153265180975410, 0.723338119937986, 1.647711436716090,
            -0.898869823857870};
        AuxiliaryRegression areg = new AuxiliaryRegression();
        areg.setSeries(series);
        areg.setDiffference(Difference.LEVEL);
        areg.setLagStructure(1, 1);
        double actual, expected;
        areg.setDeterministic(Deterministic.NONE);
        areg.estimate();
        actual = areg.getTestStat();
        expected = -2.339543595870347;
        assertEquals(expected, actual, 1e-15);
        areg.setDeterministic(Deterministic.INTERCEPT);
        areg.estimate();
        actual = areg.getTestStat();
        expected = -2.23718713839827;
        assertEquals(expected, actual, 1e-15);
        areg.setDeterministic(Deterministic.TREND_AND_INTERCEPT);
        areg.estimate();
        actual = areg.getTestStat();
        expected = -2.45119215771151;
        assertEquals(expected, actual, 1e-14);
    }

    @Test
    public void testCase06() {
        double[] series = new double[]{-0.062680699090387, 0.400542667660160,
            -0.718787981053512, 0.761355647624008, 0.158482971052969,
            0.004704836649814, -3.565640922750250, -0.342422900884603,
            1.054734493805940, -0.413128410833109, 1.392852020441280,
            1.153265180975410, 0.723338119937986, 1.647711436716090,
            -0.898869823857870};
        AuxiliaryRegression areg = new AuxiliaryRegression();
        areg.setSeries(series);
        areg.setDiffference(Difference.LEVEL);
        areg.setLagStructure(2, 2);
        double actual, expected;
        areg.setDeterministic(Deterministic.NONE);
        areg.estimate();
        actual = areg.getTestStat();
        expected = -1.664364443400673;
        assertEquals(expected, actual, 1e-14);
        areg.setDeterministic(Deterministic.INTERCEPT);
        areg.estimate();
        actual = areg.getTestStat();
        expected = -1.592779065030777;
        assertEquals(expected, actual, 1e-14);
        areg.setDeterministic(Deterministic.TREND_AND_INTERCEPT);
        areg.estimate();
        actual = areg.getTestStat();
        expected = -1.703923821756938;
        assertEquals(expected, actual, 1e-14);
    }

    @Test
    public void testCase07() {
        double[] series = new double[]{-0.062680699090387, 0.400542667660160,
            -0.718787981053512, 0.761355647624008, 0.158482971052969,
            0.004704836649814, -3.565640922750250, -0.342422900884603,
            1.054734493805940, -0.413128410833109, 1.392852020441280,
            1.153265180975410, 0.723338119937986, 1.647711436716090,
            -0.898869823857870};
        AuxiliaryRegression areg = new AuxiliaryRegression();
        areg.setSeries(series);
        areg.setDiffference(Difference.LEVEL);
        areg.setLagStructure(3, 3);
        double actual, expected;
        areg.setDeterministic(Deterministic.NONE);
        areg.estimate();
        actual = areg.getTestStat();
        expected = -1.611138714861633;
        assertEquals(expected, actual, 1e-14);
        areg.setDeterministic(Deterministic.INTERCEPT);
        areg.estimate();
        actual = areg.getTestStat();
        expected = -1.492331866397916;
        assertEquals(expected, actual, 1e-14);
        areg.setDeterministic(Deterministic.TREND_AND_INTERCEPT);
        areg.estimate();
        actual = areg.getTestStat();
        expected = -1.855477250413392;
        assertEquals(expected, actual, 1e-14);
    }

    @Test
    public void testCase08() {
        double[] series = new double[]{-0.062680699090387, 0.400542667660160,
            -0.718787981053512, 0.761355647624008, 0.158482971052969,
            0.004704836649814, -3.565640922750250, -0.342422900884603,
            1.054734493805940, -0.413128410833109, 1.392852020441280,
            1.153265180975410, 0.723338119937986, 1.647711436716090,
            -0.898869823857870};
        AuxiliaryRegression areg = new AuxiliaryRegression();
        areg.setSeries(series);
        areg.setDiffference(Difference.LEVEL);
        areg.setLagStructure(4, 4);
        double actual, expected;
        areg.setDeterministic(Deterministic.NONE);
        areg.estimate();
        actual = areg.getTestStat();
        expected = -1.280401038517989;
        assertEquals(expected, actual, 1e-14);
        areg.setDeterministic(Deterministic.INTERCEPT);
        areg.estimate();
        actual = areg.getTestStat();
        expected = -1.138098413876356;
        assertEquals(expected, actual, 1e-14);
        areg.setDeterministic(Deterministic.TREND_AND_INTERCEPT);
        areg.estimate();
        actual = areg.getTestStat();
        expected = -1.619543789083707;
        assertEquals(expected, actual, 1e-14);
    }
}
