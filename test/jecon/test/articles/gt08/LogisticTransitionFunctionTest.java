package jecon.test.articles.gt08;

import org.junit.Test;
import static org.junit.Assert.*;
import jecon.main.articles.gt08.LogisticTransitionFunction;

/**
 * unit test for <code>LogisticTransitionFunction</code> class
 *
 * @author Tamer Kulaksizoglu
 */
public class LogisticTransitionFunctionTest {

    @Test
    public void testCase01() {
        LogisticTransitionFunction ltf = new LogisticTransitionFunction();
        ltf.setLocationParameter(0.5);
        ltf.setSampleSize(10);
        ltf.setSmoothnessParameter(1);
        double[] expected = new double[]{0.210625943347236, 0.270740786206406,
            0.340609725633757, 0.418170275151203, 0.500000000000000, 0.581829724848797,
            0.659390274366243, 0.729259213793594, 0.789374056652764, 0.839086338128553};
        for (int i = 0; i < expected.length; i++) {
            double actual = ltf.valueAt(i + 1);
            assertEquals(expected[i], actual, 1e-15);
        }
    }

    @Test
    public void testCase02() {
        LogisticTransitionFunction ltf = new LogisticTransitionFunction();
        ltf.setLocationParameter(0.9);
        ltf.setSampleSize(10);
        ltf.setSmoothnessParameter(10);
        double[] expected = new double[]{0.000000000003346, 0.000000000090993,
            0.000000002474202, 0.000000067276386, 0.000001829318599, 0.000049738880531,
            0.001350697435280, 0.035472133921610, 0.500000000000000, 0.964527866078390};
        for (int i = 0; i < expected.length; i++) {
            double actual = ltf.valueAt(i + 1);
            assertEquals(expected[i], actual, 1e-15);
        }
    }

    @Test
    public void testCase03() {
        LogisticTransitionFunction ltf = new LogisticTransitionFunction();
        ltf.setLocationParameter(0.1);
        ltf.setSampleSize(10);
        ltf.setSmoothnessParameter(0.1);
        double[] expected = new double[]{0.500000000000000, 0.508256477663216,
            0.516508453839314, 0.524751436852791, 0.532980954605963, 0.541192564254711,
            0.549381861749437, 0.557544491197951, 0.565676154008427, 0.573772617772281};
        for (int i = 0; i < expected.length; i++) {
            double actual = ltf.valueAt(i + 1);
            assertEquals(expected[i], actual, 1e-15);
        }
    }
}
