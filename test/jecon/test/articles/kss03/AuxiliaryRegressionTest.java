package jecon.test.articles.kss03;

import org.junit.Test;
import static org.junit.Assert.*;
import jecon.main.articles.kss03.AuxiliaryRegression;
import jecon.main.articles.kss03.Transformation;

/**
 * unit test for AuxiliaryRegression class
 *
 * @author Tamer Kulaksizoglu
 */
public class AuxiliaryRegressionTest {

    AuxiliaryRegression ar;

    public AuxiliaryRegressionTest() {
        ar = new AuxiliaryRegression();
        ar.setSeries(new double[]{1, 2, 2, 3, 2, 3, 2, 4, 5, 4, 5, 6});
    }

    @Test
    public void test01() {
        ar.setLagStructure(0, 0);
        ar.setTransformation(Transformation.NONE);
        ar.estimate();
        double actual = 0.467613753866953;
        double expected = ar.getTStat();
        assertEquals(expected, actual, 1e-15);
    }

    @Test
    public void test02() {
        ar.setLagStructure(1, 1);
        ar.setTransformation(Transformation.NONE);
        ar.estimate();
        double actual = 1.224035114753412;
        double expected = ar.getTStat();
        assertEquals(expected, actual, 1e-15);
    }

    @Test
    public void test03() {
        ar.setLagStructure(2, 2);
        ar.setTransformation(Transformation.NONE);
        ar.estimate();
        double actual = 1.370098154816723;
        double expected = ar.getTStat();
        assertEquals(expected, actual, 1e-15);
    }

    @Test
    public void test04() {
        ar.setLagStructure(0, 0);
        ar.setTransformation(Transformation.DEMEAN);
        ar.estimate();
        double actual = -1.208369557590098;
        double expected = ar.getTStat();
        assertEquals(expected, actual, 1e-15);
    }

    @Test
    public void test05() {
        ar.setLagStructure(1, 1);
        ar.setTransformation(Transformation.DEMEAN);
        ar.estimate();
        double actual = -0.293216730910639;
        double expected = ar.getTStat();
        assertEquals(expected, actual, 1e-15);
    }

    @Test
    public void test06() {
        ar.setLagStructure(2, 2);
        ar.setTransformation(Transformation.DEMEAN);
        ar.estimate();
        double actual = -0.183258376280399;
        double expected = ar.getTStat();
        assertEquals(expected, actual, 1e-15);
    }

    @Test
    public void test07() {
        ar.setLagStructure(0, 0);
        ar.setTransformation(Transformation.DETREND);
        ar.estimate();
        double actual = -2.559998308747343;
        double expected = ar.getTStat();
        assertEquals(expected, actual, 1e-15);
    }

    @Test
    public void test08() {
        ar.setLagStructure(1, 1);
        ar.setTransformation(Transformation.DETREND);
        ar.estimate();
        double actual = -1.583923847322732;
        double expected = ar.getTStat();
        assertEquals(expected, actual, 1e-14);
    }

    @Test
    public void test09() {
        ar.setLagStructure(2, 2);
        ar.setTransformation(Transformation.DETREND);
        ar.estimate();
        double actual = -1.206937380736553;
        double expected = ar.getTStat();
        assertEquals(expected, actual, 1e-14);
    }
}
