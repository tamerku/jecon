package jecon.test.articles.lnv98;

import org.junit.Test;
import static org.junit.Assert.*;
import jecon.main.articles.lnv98.LogisticTransitionFunction;

/**
 * unit test for <code>LogisticTransitionFunction</code> class
 *
 * @author Tamer Kulaksizoglu
 */
public class LogisticTransitionFunctionTest {

    @Test
    public void testCase01() {
        LogisticTransitionFunction ltf = new LogisticTransitionFunction();
        ltf.setGamma(1);
        ltf.setTau(0.5);
        ltf.setSampleSize(10);
        double[] expected = new double[]{0.017986209962092, 0.047425873177567,
            0.119202922022118, 0.268941421369995, 0.500000000000000, 0.731058578630005,
            0.880797077977882, 0.952574126822433, 0.982013790037908, 0.993307149075715};
        double[] actual = new double[10];
        for (int i = 0; i < ltf.getSampleSize(); i++) {
            actual[i] = ltf.valueAt(i + 1);
        }
        assertArrayEquals(expected, actual, 1e-15);
    }

    @Test
    public void testCase02() {
        LogisticTransitionFunction ltf = new LogisticTransitionFunction();
        ltf.setGamma(0.1);
        ltf.setTau(0.5);
        ltf.setSampleSize(10);
        double[] expected = new double[]{0.401312339887548, 0.425557483188341,
            0.450166002687522, 0.475020812521060, 0.500000000000000, 0.524979187478940,
            0.549833997312478, 0.574442516811659, 0.598687660112452, 0.622459331201855};
        double[] actual = new double[10];
        for (int i = 0; i < ltf.getSampleSize(); i++) {
            actual[i] = ltf.valueAt(i + 1);
        }
        assertArrayEquals(expected, actual, 1e-15);
    }

    @Test
    public void testCase03() {
        LogisticTransitionFunction ltf = new LogisticTransitionFunction();
        ltf.setGamma(15);
        ltf.setTau(0.5);
        ltf.setSampleSize(10);
        double[] expected = new double[]{0.000000000000000, 0.000000000000000,
            0.000000000000094, 0.000000305902227, 0.500000000000000, 0.999999694097773,
            0.999999999999907, 1.000000000000000, 1.000000000000000, 1.000000000000000};
        double[] actual = new double[10];
        for (int i = 0; i < ltf.getSampleSize(); i++) {
            actual[i] = ltf.valueAt(i + 1);
        }
        assertArrayEquals(expected, actual, 1e-15);
    }

    @Test
    public void testCase04() {
        LogisticTransitionFunction ltf = new LogisticTransitionFunction();
        ltf.setGamma(0.5);
        ltf.setTau(0.2);
        ltf.setSampleSize(25);
        double[] expected = new double[]{0.119202922022118, 0.182425523806356,
            0.268941421369995, 0.377540668798145, 0.500000000000000, 0.622459331201855,
            0.731058578630005, 0.817574476193644, 0.880797077977882, 0.924141819978757,
            0.952574126822433, 0.970687769248644, 0.982013790037908, 0.989013057369407,
            0.993307149075715, 0.995929862284104, 0.997527376843365, 0.998498817743263,
            0.999088948805599, 0.999447221363076, 0.999664649869534, 0.999796573021945,
            0.999876605424014, 0.999925153772489, 0.999954602131298};
        double[] actual = new double[25];
        for (int i = 0; i < ltf.getSampleSize(); i++) {
            actual[i] = ltf.valueAt(i + 1);
        }
        assertArrayEquals(expected, actual, 1e-15);
    }

    @Test
    public void testCase05() {
        LogisticTransitionFunction ltf = new LogisticTransitionFunction();
        ltf.setGamma(0.5);
        ltf.setTau(0.8);
        ltf.setSampleSize(25);
        double[] expected = new double[]{0.000074846227511, 0.000123394575986,
            0.000203426978055, 0.000335350130466, 0.000552778636924, 0.000911051194401,
            0.001501182256737, 0.002472623156635, 0.004070137715896, 0.006692850924285,
            0.010986942630593, 0.017986209962092, 0.029312230751356, 0.047425873177567,
            0.075858180021244, 0.119202922022118, 0.182425523806356, 0.268941421369995,
            0.377540668798145, 0.500000000000000, 0.622459331201855, 0.731058578630005,
            0.817574476193644, 0.880797077977882, 0.924141819978757};
        double[] actual = new double[25];
        for (int i = 0; i < ltf.getSampleSize(); i++) {
            actual[i] = ltf.valueAt(i + 1);
        }
        assertArrayEquals(expected, actual, 1e-15);
    }

    @Test
    public void testCase06() {
        LogisticTransitionFunction ltf = new LogisticTransitionFunction();
        ltf.setGamma(1);
        ltf.setTau(0.2);
        ltf.setSampleSize(25);
        double[] expected = new double[]{0.017986209962092, 0.047425873177567,
            0.119202922022118, 0.268941421369995, 0.500000000000000, 0.731058578630005,
            0.880797077977882, 0.952574126822433, 0.982013790037908, 0.993307149075715,
            0.997527376843365, 0.999088948805599, 0.999664649869534, 0.999876605424014,
            0.999954602131298, 0.999983298578152, 0.999993855825398, 0.999997739675702,
            0.999999168471972, 0.999999694097773, 0.999999887464838, 0.999999958600624,
            0.999999984770021, 0.999999994397204, 0.999999997938846};
        double[] actual = new double[25];
        for (int i = 0; i < ltf.getSampleSize(); i++) {
            actual[i] = ltf.valueAt(i + 1);
        }
        assertArrayEquals(expected, actual, 1e-15);
    }

    @Test
    public void testCase07() {
        LogisticTransitionFunction ltf = new LogisticTransitionFunction();
        ltf.setGamma(0.8);
        ltf.setTau(0.5);
        ltf.setSampleSize(10);
        double[] derivatives = ltf.derivativeAt(1);
        double expected = -0.1505270758182856;
        double actual = derivatives[0];
        assertEquals(expected, actual, 1e-15);
        expected = -0.3010541516365712;
        actual = derivatives[1];
        assertEquals(expected, actual, 1e-15);
    }

}
