package jecon.test.articles.lnv98;

import org.apache.commons.math3.linear.RealVector;
import org.junit.Test;
import static org.junit.Assert.*;
import jecon.main.articles.lnv98.AbstractModel;
import jecon.main.articles.lnv98.ModelB;

/**
 * unit test for class ModelB
 *
 * @author Tamer Kulaksizoglu
 */
public class ModelBTest {

    private double[] y;

    public ModelBTest() {
        y = new double[]{-0.414164279391877, 0.121967122632099, -0.342714341105782,
            0.260733235271576, -0.464014142183832, -0.365964449231641, 1.431021966850868,
            -0.495700463297570, 0.591433739606565, -1.467030633718221, -0.053728092627814,
            -0.035003903115377, 0.415954582334783, -0.446945753003113, -0.348163413707550,
            0.890350701251178, -0.544577449958850, 2.068205357600298, 1.864340800507192,
            -0.529176705782218, -1.018380641559292, -1.353526694743543, 1.094207535968619,
            1.453536538960765, -0.023521016326818, 0.666217145080649, -2.324858678360610,
            0.272724920693338, -1.008543177090965, -0.420499537214630, 0.596063337203513,
            0.891457355655743, 0.140167481707882, -1.186040694315125, 0.839181836350076,
            -0.441889234806812, -1.027451963956831, -0.520593201196207, 1.028934876552573,
            0.062957444892889, 0.219860696599820, -0.168092309626025, -1.218728885608120,
            -1.553388419179217, -2.664411962862781, -0.630944311378946, -2.141139205620731,
            -0.887697605350100, -1.465842069528950, -1.966649145080584};
    }

    @Test
    public void testCase01() {
        AbstractModel mod = new ModelB(1e-10);
        mod.setResponseVariable(y);
        mod.setGammaGrid(0.1, 5, 30);
        mod.setTauGrid(0.01, 0.99, 30);
        mod.estimate(null);
        RealVector actual = mod.getPar();
        double[] expected = new double[]{0.007677804295657, -1.611866235039452,
            -0.000290966925785, 3.453396214759210, 0.853461939392865};
        for (int i = 0; i < expected.length; i++) {
            assertEquals(expected[i], actual.getEntry(i), 1e-4);
        }
    }

    @Test
    public void testCase02() {
        AbstractModel mod = new ModelB(1e-12);
        mod.setResponseVariable(y);
        mod.setGammaGrid(0.1, 5, 30);
        mod.setTauGrid(0.01, 0.99, 30);
        mod.estimate(null);
        double actual = mod.calculateRSS();
        double expected = 37.374286331370143;
        assertEquals(expected, actual, 1e-9);
    }

}
