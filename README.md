## JEcon
A Java Library for Econometric Analysis

## Description
JEcon is a Java library for econometric analysis.

## Citation
If you use the library in your work, please cite it as follows:

	@misc{JEcon,
		author = "Tamer Kulaksizoglu",
		title = "JEcon",
		howpublished = "\url{https://bitbucket.org/tamerku/jecon}",
	}

## Dependencies
- [Apache Commons Math Library](http://commons.apache.org/proper/commons-math/)
